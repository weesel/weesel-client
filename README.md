## Disclaimer ##
This is project is intended as a school project.
### Installation Guide
**Note:** Please run the backend-server first before doing ```npm-start```.

```bash
    npm install
    npm start
```
### Author
- **[Sokleng Prom](https://gitlab.com/sokLeng-Prom)**
- **[Pheakdey Khen](https://gitlab.com/khenpheakdey)**
- **[Uy Seng](https://gitlab.com/nova44056)**

### License
Copyright &copy; 2021, **Team Weesel**, Paragon International University
