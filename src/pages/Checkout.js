import React, { useState, useEffect } from "react";
import CategoryList from "components/CategoryList";
import "css/Checkout.css";

import LoginForm from "components/Forms/Form";
import { loginFormSchema } from "utils/FormSchema";
import { fetchAuthenticatedUser } from "utils/const";
import { selectAccessToken, loginWithRedirect, selectUser } from "slices/auth";
import { selectRegisterData } from "slices/register";

import { useSelector, useDispatch } from "react-redux";
import RegisterGuestUserForm from "components/Forms/RegisterGuestUserForm";
import Receipt from "components/Checkout/Receipt";
import { Link, useHistory, useLocation } from "react-router-dom";
import { selectCart, clearCart } from "slices/cart";
import axios from "axios";

function Checkout() {
  const [step, setStep] = useState(1);
  const [account, setAccount] = useState();
  const user = useSelector(selectUser);
  const access_token = useSelector(selectAccessToken);
  const nextStep = () => setStep((prevState) => prevState + 1);
  const [paymentMethod, setPaymentMethod] = useState("cash");
  const [abaTransactionID, setABATransactionID] = useState("");
  const dispatch = useDispatch();
  useEffect(() => {
    if (user && step === 1) {
      setStep(2);
    }
  }, [user]);

  const registerData = useSelector(selectRegisterData);
  const myCart = useSelector(selectCart);
  const history = useHistory();

  const checkoutHandler = () => {
    const formData = user
      ? {
          buyer_id: user.id,
          payment_method: paymentMethod,
          aba_transaction_id: abaTransactionID,
          products: Object.keys(myCart).map((id) => {
            return {
              id: id,
              quantity: myCart[id],
            };
          }),
        }
      : {
          ...registerData,
          payment_method: paymentMethod,
          aba_transaction_id: abaTransactionID,
          products: Object.keys(myCart).map((id) => {
            return {
              id: id,
              quantity: myCart[id],
            };
          }),
        };
    axios
      .post("/orders", formData)
      .then(() => {
        dispatch(clearCart());
        history.push("/");
      })
      .catch((error) => console.log(error.response));
  };

  return (
    <div className="checkout">
      <div className="checkout-layout">
        <div className="checkout-categories">
          <CategoryList />
        </div>
        <div className="checkout-process">
          <div className="checkout-process-step">
            <div className="checkout-process-step-header">
              <span>Step 1: Checkout Options</span>
              {step === 1 && <StepOne nextStep={nextStep} />}
            </div>
          </div>
          <div className="checkout-process-step">
            <div className="checkout-process-step-header">
              {user ? (
                <span>Step 2: Delivery Address</span>
              ) : (
                <span>Step 2: User & Delivery Information</span>
              )}
              {user
                ? step > 1
                  ? Object.keys(user).length > 0 && (
                      <StepTwoAuthenticated
                        step={step}
                        nextStep={nextStep}
                        user_address={user.address_1}
                      />
                    )
                  : null
                : step > 1 && (
                    <StepTwoUnauthenticated step={step} nextStep={nextStep} />
                  )}
            </div>
          </div>
          <div className="checkout-process-step">
            <div className="checkout-process-step-header">
              <span>Step 3: Payment Method</span>
              {step > 2 && (
                <StepThree
                  setPaymentMethod={(method) => setPaymentMethod(method)}
                  setABATransactionID={(id) => setABATransactionID(id)}
                  step={step}
                  nextStep={nextStep}
                />
              )}
            </div>
          </div>
          <div className="checkout-process-step">
            <div className="checkout-process-step-header">
              <span>Step 4: Confirm Order</span>
              {step > 3 && (
                <React.Fragment>
                  <Receipt deliveryPrice={Math.random(1, 5).toFixed(2)} />
                  <button
                    onClick={checkoutHandler}
                    className="checkout-primary-btn"
                  >
                    I have confirmed my order
                  </button>
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const StepOne = ({ nextStep }) => {
  const { pathname } = useLocation();
  const history = useHistory();
  const dispatch = useDispatch();
  const submitHandler = (loginCredentials) =>
    dispatch(
      loginWithRedirect({
        loginCredentials: loginCredentials,
        redirectCallback: () => history.push(pathname),
      })
    );
  return (
    <div className="checkout-process-step-info-1">
      <div className="checkout-process-step-info-1-left">
        <p className="checkout-process-step-info-title">Guest Customer</p>
        <p>
          By{" "}
          <Link style={{ fontWeight: "700" }} to="/register">
            creating an account
          </Link>{" "}
          you will be able to shop faster, be up to date on an order's status,
          and keep track of the orders you have previously made.
        </p>
        <button onClick={() => nextStep()} className="checkout-primary-btn">
          Go to next step
        </button>
      </div>
      <div className="checkout-process-step-info-1-right">
        <p className="checkout-process-step-info-title">Returning Customer</p>
        <p className="checkout-process-step-info-subtitle">
          I am a returning customer
        </p>
        <LoginForm
          fields={loginFormSchema}
          submitHandler={submitHandler}
          children={
            <button className="login-form-btn" type="submit">
              Login
            </button>
          }
        />
      </div>
    </div>
  );
};

const StepTwoAuthenticated = ({ step, user_address, nextStep }) => {
  const [address, setAddress] = useState("existing");
  return (
    <div className="checkout-process-step-info-2">
      <div>
        <input
          type="radio"
          name="billing-address"
          id="existing-address"
          defaultChecked
        />
        <label htmlFor="existing-address">{user_address}</label>
        {/* {address === "existing" && <div>{user_address}</div>} */}
        {/* <input type="radio" name="billing-address" id="new-address" />
        <label htmlFor="new-address">I want to use a new address</label>
        <input type="text" name="new-address" id="new-address" /> */}
      </div>
      {step === 2 && (
        <button onClick={() => nextStep()} className="checkout-primary-btn">
          Continue
        </button>
      )}
    </div>
  );
};

const StepTwoUnauthenticated = ({ step, nextStep }) => {
  const registerData = useSelector(selectRegisterData);
  return (
    <React.Fragment>
      <RegisterGuestUserForm />
      {registerData.address_1 && step === 2 && (
        <button className="checkout-primary-btn" onClick={() => nextStep()}>
          Go to next step
        </button>
      )}
    </React.Fragment>
  );
};

const StepThree = ({
  nextStep,
  step,
  setPaymentMethod,
  setABATransactionID,
}) => {
  const [options, setOptions] = useState("cash");
  return (
    <div className="checkout-process-step-info-2">
      <p>Please select the prefered payment method</p>
      <div className="checkout-process-step-info-2-section">
        <div>
          <input
            type="radio"
            name="payment-method"
            id="cash"
            value="cash"
            onChange={(e) => {
              setOptions(e.target.value);
              setPaymentMethod(e.target.value);
            }}
            defaultChecked
          />
          <label htmlFor="cash">Pay by Cash</label>
        </div>
        <div>
          <input
            type="radio"
            name="payment-method"
            value="aba"
            onChange={(e) => {
              setOptions(e.target.value);
              setPaymentMethod(e.target.value);
            }}
            id="aba"
          />
          <label htmlFor="aba">Pay by ABA (Account number: 696 969 696)</label>
        </div>
        {options === "aba" && (
          <React.Fragment>
            <label htmlFor="transaction_id">Transaction ID (Trx. ID):</label>
            <input
              onChange={(e) => setABATransactionID(e.target.value)}
              type="text"
              name="transaction-id"
              id="transaction-id"
            />
          </React.Fragment>
        )}
      </div>
      {step === 3 && (
        <button className="checkout-primary-btn" onClick={() => nextStep()}>
          Continue
        </button>
      )}
    </div>
  );
};

export default Checkout;
