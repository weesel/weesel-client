import React, { useEffect, useState, useRef } from "react";
import { Link, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import CategoryList from "components/CategoryList";
import MiniCarousel from "components/MiniCarousel";
import {
  fetchProduct,
  fetchProductFromSeller,
  fetchCategoryProducts,
} from "utils/const";
import { addToCart } from "slices/cart";
import "css/ProductDetail.css";
import Rating from "components/Rating";
import ProductCard from "components/ProductCard";
import ProductCarousel from "components/Home/ProductCarousel";
import ReactOwlCarousel from "react-owl-carousel";

const reactOwlCarouselOptions = {
  responsive: {
    300: {
      items: 1,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 4,
    },
  },
  margin: 0,
  dots: false,
};

function ProductDetail() {
  const { id } = useParams();
  const [categoryId, setCategoryId] = useState([]);
  const [category, setCategory] = useState([]);
  const [product, setProduct] = useState({});
  const [productImages, setProductImages] = useState([]);
  const dispatch = useDispatch();
  const [productFromSeller, setproductFromSeller] = useState([]);
  const [relatedProduct, setRelatedProduct] = useState([]);
  const productCarousel = useRef();

  useEffect(() => {
    fetchProduct(id)
      .then((product) => {
        setProduct(product);
        setProductImages((prevState) => {
          prevState = product.images.map((image) => image.url);
          return prevState;
        });
      })
      .catch((error) => console.log(error));
  }, []);

  useEffect(() => {
    if (categoryId[1] && relatedProduct.length === 0) {
      fetchCategoryProducts(categoryId[1])
        .then((product) => {
          setRelatedProduct(product);
        })
        .catch((error) => console.log(error));
    }else if(categoryId[0] && relatedProduct.length === 0){
       fetchCategoryProducts(categoryId[0])
        .then((product) => {
          setRelatedProduct(product);
        })
        .catch((error) => console.log(error));
    }
  },[categoryId[0]]);

  console.log(relatedProduct);

  useEffect(() => {
    if (product.seller_id && productFromSeller.length === 0) {
      fetchProductFromSeller(product.seller_id)
        .then((product) => {
          setproductFromSeller(product);
        })
        .catch((error) => console.log(error));
    }
  }, [product.seller_id]);

  useEffect(() => {
    fetchProduct(id)
      .then((product) => {
        setCategory((prevState) => {
          prevState = product.categories.map((cat) => cat.name);
          return prevState;
        });
        setCategoryId((prevState) => {
          prevState = product.categories.map((cate) => cate.id);
          return prevState;
        });
      })
      .catch((error) => console.log(error));
  }, []);

  return (
    <div className="col-md-12 col-sm-12 productDetail">
      {product.id && (
        <React.Fragment>
          <div className="productDetail-header">
            <div className="productDetail-header-title">
              <Link to={`/`}>
                <p>Home</p>
              </Link>
              <p>{">"}</p>
              <Link>
                <p>{category[0]}</p>
              </Link>
              <p>{">"}</p>
              <p>{category[1]}</p>
              <p>{">"}</p>
              <p>{product.name}</p>
            </div>
          </div>
          <div className="productDetail-body">
            <div className="categoryList">
              <CategoryList />
            </div>
            <div className="productDetail-detail">
              <div className="productDetail-images">
                <div className="productDetail-images-single">
                  <img src={productImages[0]} alt="product" />
                </div>
                {productImages.length > 0 ? 
                  <div className="productDetail-images-many">
                    <MiniCarousel
                      images={productImages.splice(1, productImages.length - 1)}
                    />
                  </div> : <div>Noo</div>
                }
              </div>
              <div className="productDetail-info">
                <div className="productDetail-info-title">
                  <span>{product.name}</span>
                </div>
                <hr />
                <div>
                  <div className="productDetail-info-id">
                    <div>
                      <p>Product ID:</p>
                      <span>{product.id}</span>
                    </div>
                    <div>
                      <p>Seller ID:</p>
                      <span>{product.seller_id}</span>
                    </div>
                  </div>
                </div>
                <hr />
                {/* <div className="product-info-price">
                    <span>Price: </span>
                  </div>
                  <div className="price-line-through">
                    <span>${product.price.toFixed(2)}</span>
                  </div>
                  <div className="price-after-discount">
                    <span>
                      $
                      {(
                        product.price -
                        (product.price * product.discount) / 100
                      ).toFixed(2)}
                    </span>
                  </div> */}
                <div className="productDetail-add-to-cart">
                  <div className="productQuantity">
                    <label htmlFor="qty">Qty:</label>
                    <input defaultValue="1" type="number" name="qty" id="qty" />
                  </div>
                  <button
                    className="product-btn"
                    onClick={(e) => {
                      const amount = parseInt(
                        e.currentTarget.previousSibling.querySelector("#qty")
                          .value,
                        10
                      );
                      dispatch(
                        addToCart({ productID: product.id, amount: amount })
                      );
                    }}
                  >
                    <span>Add To Cart</span>
                    <i className="fas fa-shopping-cart"></i>
                  </button>
                </div>
              </div>
              <div className="product-avaibility">
                <div>
                  <div>Availability:</div>
                  {product.quantity > 0 ? <span className="instock">In Stock</span>:<span className="outstock">Out of Stock</span>}
                </div>
                <hr />
                <div>
                  {product.discount === 0 ? (
                    <div className="discount-price">
                      <div className="product-info-price">
                        <div>
                          <span>${product.price.toFixed(2)}</span>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div className="discount-price">
                      <div className="price-line-through">
                        <span>${product.price.toFixed(2)}</span>
                      </div>
                      <div className="price-after-discount">
                        <span>
                          $
                          {(
                            product.price -
                            (product.price * product.discount) / 100
                          ).toFixed(2)}
                        </span>
                      </div>
                    </div>
                  )}
                </div>
                <div>
                  <div>
                    <Rating rating={product.rating} />
                  </div>
                  <p>
                    <a href="#">Write review</a>
                  </p>
                </div>
              </div>
            </div>
            <div />
            <div className="product-Description">
              <span style={{ borderBottom: "4px solid #ffba00" }}>
                Description
              </span>
              <p>{product.description}</p>
            </div>
          </div>
        </React.Fragment>
      )}
      <div className="productRelated">
        <div className="productRelated-tab"> <p>Products From Seller</p></div>
        <div className="productRelated-products">
          {productFromSeller &&
            productFromSeller.map((product) => (
              <Link to={`/product/${product.id}`}>
                <ProductCard
                  title={product.name}
                  rating={product.rating}
                  price={product.price}
                  discount={product.discount}
                  image={product.images[0].url}
                />
              </Link>
            ))}
        </div>
        <div className="productRelated-tab"> <p>Related Products</p></div>
        <div className="productRelated-products">
          {relatedProduct.map((product)=>(
            <Link to={`/product/${product.id}`}>
            <ProductCard title={product.name}
            image={product.images[0].url}
            rating={product.rating}
            price={product.price}
            discount={product.discount}/>
            </Link>
            ))}
        </div>
      </div>
    </div>
  );
}

export default ProductDetail;
