import React, { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import queryString from "query-string";
import { fetchProductsWithQuery } from "utils/const";
import ProductCard from "components/ProductCard";
import "css/ProductResult.css";

function ProductResult() {
  const { search } = useLocation();
  const query = queryString.parse(search);
  const [products, setProducts] = useState([]);
  const [searchMessage, setSearchMessage] = useState("");
  useEffect(() => {
    console.log(query);
    fetchProductsWithQuery(query)
      .then((products) => {
        if (products.length === 0) setSearchMessage("No Results Found");
        setProducts(products);
      })
      .catch((error) => console.log(error));
  }, []);

  return Object.keys(query).length > 0 ? (
    <div style={{ minHeight: "63.7vh" }}>
      <div className="product-result-title">
        <h1>{searchMessage.length > 0 ? searchMessage : "Product Results"}</h1>
        <i className="fas fa-leaf"></i>
      </div>
      <div className="product-result">
        <div className="product-result-grid">
          {products.map((product) => (
            <Link to={`/product/${product.id}`}>
              <ProductCard
                title={product.name}
                image={product.images[0].url}
                rating={product.rating}
                price={product.price}
                key={product.id}
                discount={product.discount}
                image={product.images[0].url}
              />
            </Link>
          ))}
        </div>
      </div>
    </div>
  ) : (
    <div>Error</div>
  );
}

export default ProductResult;
