import React, { useState, useEffect } from "react";
import {
  fetchAuthenticatedUser,
  fetchUserOrder,
  capitalizeFirstLetter,
} from "utils/const";
import { selectAccessToken, selectUser } from "slices/auth";
import { useSelector } from "react-redux";
import "css/Myprofile.css";
import axios from "axios";

// const fetchUser = async (id) => {
//   return axios
//     .get(`/users/${id}`)
//     .then((response) => response.data.data)
//     .catch((error) => error);
// };

function Myprofile() {
  const access_token = useSelector(selectAccessToken);
  const user = useSelector(selectUser);
  const [orders, setOrders] = useState([]);
  const [users,setUsers] = useState([]);

  useEffect(() => {
    // if (Object.keys(user).length > 0) {
    // }
    // fetchUser(user).then((response)=>{setUsers(response)}).catch((error)=>console.log(error))
    setUsers(user);
  }, [user]);
  console.log(user);
    console.log(users);
  return (
    <div className="myprofile">
      <h1>Order History</h1>
      <div>
        {orders.map((order) => (
          <p key={order.id}>Order ID: #{order.id}</p>
        ))}
      </div>
      <h1>Account Information</h1>
      <div className="user-profile">
        <div>
          
        </div>
      </div>
    </div>
  );
}

export default Myprofile;
