import React from "react";
import CategoryList from "components/CategoryList";
import { useHistory } from "react-router-dom";
import "css/MyCart.css";
import CartTable from "components/MyCart/CartTable";

function MyCart() {
  const history = useHistory();

  return (
    <div className="mycart">
      <div className="mycart-layout">
        <div className="mycart-categories">
          <CategoryList />
        </div>
        <div className="mycart-product">
          <CartTable />
          <div className="mycart-checkout">
            <button
              onClick={() => history.push("/checkout")}
              className="mycart-checkout-btn"
              type="button"
            >
              <i className="fas fa-shopping-cart"></i>
              <span>Checkout</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MyCart;
