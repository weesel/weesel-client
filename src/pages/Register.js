import React, { useRef, useState } from "react";
import axios from "axios";
import RegisterForm from "components/Forms/Form";
import {
  registerFormUserSectionSchema,
  registerFormAddressSectionSchema,
} from "utils/FormSchema";
import { useHistory } from "react-router-dom";
import CategoryList from "components/CategoryList";
import "css/Register.css";
import { useSelector, useDispatch } from "react-redux";
import { selectRegisterData, setRegisterData } from "slices/register";

function Register() {
  const history = useHistory();

  const registerData = useSelector(selectRegisterData);
  const dispatch = useDispatch();
  const [toastMessage, setToastMessage] = useState("");
  const register = (e) => {
    e.preventDefault();
    console.log(registerData);
    axios
      .post("/auth/register", registerData)
      .then((response) => {
        console.log(response);
        setTimeout(() => {
          setToastMessage("");
        }, 2000);
        setToastMessage("Registration Successful");
      })
      .catch((error) => {
        setToastMessage(`Registration Failed`);
        console.log(error);
      });
  };

  const formHandler = (payload) => dispatch(setRegisterData(payload));

  return (
    <div className="register">
      {toastMessage.length > 0 && (
        <div className="register-toast">{toastMessage}</div>
      )}
      <div className="register-layout">
        <div className="register-categories">
          <CategoryList />
        </div>
        <div className="register-form">
          <div className="register-form-section">
            <p className="register-form-title">User Information</p>
            <RegisterForm
              fields={registerFormUserSectionSchema}
              submitHandler={formHandler}
              children={
                !registerData.name && (
                  <button className="register-form-btn" type="submit">
                    Continue
                  </button>
                )
              }
            />
          </div>
          <div className="register-form-section">
            <p className="register-form-title">User Address</p>
            {registerData.name && (
              <React.Fragment>
                <RegisterForm
                  fields={registerFormAddressSectionSchema}
                  submitHandler={formHandler}
                  children={
                    !registerData.address_1 && (
                      <button className="register-form-btn" type="submit">
                        Continue
                      </button>
                    )
                  }
                />
              </React.Fragment>
            )}
          </div>
          <div className="register-form-section">
            <p className="register-form-title">Complete Registration</p>
            {registerData.address_1 && (
              <React.Fragment>
                <form onSubmit={register}>
                  <input
                    type="checkbox"
                    required
                    name="privacy-policy"
                    id="privacy-policy"
                  />
                  <label htmlFor="privact-policy">
                    I have read and agree to the Privacy Policy
                  </label>
                  <br />
                  <button className="register-form-btn" type="submit">
                    Register
                  </button>
                </form>
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
