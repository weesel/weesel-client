import React, { useState } from "react";

import LoginForm from "components/Forms/Form";
import { loginFormSchema } from "utils/FormSchema";
import { useDispatch } from "react-redux";
// import axios from "axios";
import { useHistory } from "react-router-dom";
import "css/Login.css";
import CategoryList from "components/CategoryList";
import { Link } from "react-router-dom";
import { loginWithRedirect } from "slices/auth";

function Login() {
  const dispatch = useDispatch();
  const history = useHistory();
  // const [errorMessage, setErrorMessage] = useState("");
  const submitHandler = (loginCredentials) =>
    dispatch(
      loginWithRedirect({
        loginCredentials: loginCredentials,
        redirectCallback: () => history.push("/"),
      })
    );
  return (
    <div className="login">
      {/* {errorMessage.length > 0 && (
        <div className="error-toast">{errorMessage}</div>
      )} */}
      <div className="login-layout">
        <div className="login-categories">
          <CategoryList />
        </div>
        <div className="login-options">
          <div className="login-go-to-register">
            <p className="login-go-to-register-title">New Customer</p>
            <p className="login-go-to-register-subtitle">Register Account</p>
            <p>
              By creating an account you will be able to shop faster, be up to
              date on an order's status, and keep track of the orders you have
              previously made.
            </p>
            <Link to="/register" className="login-go-to-register-btn">
              Continue
            </Link>
          </div>
          <div className="login-form">
            <p className="login-form-title">Returning Customer</p>
            <p className="login-form-subtitle">I am a returning customer</p>
            <LoginForm
              fields={loginFormSchema}
              submitHandler={submitHandler}
              children={
                <button className="login-form-btn" type="submit">
                  Login
                </button>
              }
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
