import React from "react";
import Benefit from "components/Home/Benefit";
import AdvertisementSlider from "components/Home/AdvertisementCarousel";
import CollectionWithTabs from "components/Home/CollectionWithTabs";
import Collection from "components/Home/Collection";
import CategoryList from "components/CategoryList";
import "css/Home.css";


//exchange_rate api = http://api.exchangeratesapi.io/v1/latest?access_key=75e5604dd2f05fd63c6b17dee2709cdc

function Home() {
  return (
    <div className="home">
      <div className="home-top">
        <div className="home-top-left">
          <div className="home-categories">
            <span>Categories</span>
            <CategoryList />
          </div>
        </div>
        <div className="home-top-right">
          <div className="home-top-right-advertisement">
            <AdvertisementSlider />
            <div className="home-top-right-advertisement-banner">
              <div style={{ maxWidth: "100%", height: "auto" }}>
                <img
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "fit",
                    objectPosition: "center",
                  }}
                  src="https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC05/image/catalog/demo/banners/rightbanner-01.jpg"
                  alt="banner-top"
                />
              </div>
              <div style={{ maxWidth: "100%", height: "auto" }}>
                <img
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "fit",
                    objectPosition: "center",
                  }}
                  src="https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC05/image/catalog/demo/banners/rightbanner-02.jpg"
                  alt="banner-bottom"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <CollectionWithTabs tabs={["featured", "recommended", "trending"]} />
      {/* ads banner */}
      <div
        style={{
          backgroundImage: `url(https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC05/image/catalog/demo/banners/banner-03.jpg)`,
          height: "10rem",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          marginTop: "1rem",
          backgroundSize: "contain",
        }}
      />
      <Collection title={"promotion"} />
      <div style={{ margin: "2rem 0" }}>
        <span>Benefit</span>
        <div style={{ backgroundColor: "white" }}>
          <section>
            <Benefit />
          </section>
        </div>
      </div>
    </div>
  );
}

export default Home;
