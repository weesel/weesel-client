import axios from "axios";
// export const API_URL = "https://weesel.ap-southeast-1.elasticbeanstalk.com/api";
export const API_URL = "http://127.0.0.1:8000/api";

export const fetchCategoriesWithChildren = async () => {
  return axios
    .get("/categories", {
      params: {
        include: "children",
      },
    })
    .then((response) => response.data.data)
    .catch((error) => error);
};

export const fetchParentCategories = async () => {
  return axios
    .get("/categories", {
      params: {
        show: "parent_only",
      },
    })
    .then((response) => response.data.data)
    .catch((error) => error);
};

export const fetchCategoryProducts = async (id) => {
  return axios
    .get(`/categories/${id}/products`, {
      params: {
        limit: 6,
      },
    })
    .then((response) => response.data.data.data)
    .catch((error) => error);
};

export const fetchProductCollections = async () => {
  return axios
    .get("/product-collections")
    .then((response) => response.data.data)
    .catch((error) => error);
};

export const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const fetchProduct = async (id) => {
  return axios
    .get(`/products/${id}`)
    .then((response) => response.data.data)
    .catch((error) => error);
};

export const fetchProductFromSeller = async (id) => {
  return axios
    .get(`/sellers/${id}/products`, {
      params: {
        limit: 6,
      },
    })
    .then((response) => response.data.products)
    .catch((error) => error);
};

export const fetchAuthenticatedUser = async () => {
  try {
    const response = await axios.get("/auth/me", {
      withCredentials: true,
    });
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const fetchProductsWithQuery = async (query) => {
  return axios
    .get("/products", {
      params: query,
    })
    .then((response) => response.data.data)
    .catch((error) => error);
};

export const login = async (payload) => {
  try {
    const response = await axios.post("/auth/login", payload, {
      withCredentials: true,
    });
    return response.data;
  } catch (error) {
    console.log(error.response);
    throw error;
  }
};

export const logout = async (token) => {
  return axios
    .post("/auth/logout", null, {
      withCredentials: true,
    })
    .then((response) => response.data)
    .catch((error) => error);
};

export const fetchUserOrder = async (id) => {
  return axios
    .get(`/users/${id}/orders`)
    .then((response) => response.data.data)
    .catch((error) => error);
};

export const removeAccessTokenFromStorage = () =>
  localStorage.removeItem("user_access_token");
export const setAccessTokenToStorage = (token) =>
  localStorage.setItem("user_access_token", JSON.stringify(token));
export const getAccessTokenFromStorage = () => {
  return JSON.parse(localStorage.getItem("user_access_token"));
};
