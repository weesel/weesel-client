import * as Yup from "yup";
const loginFormSchema = [
  {
    key: "email",
    value: "",
    label: "Email: ",
    control: "text",
    validation: Yup.string("Email must be a string")
      .required("Email is required")
      .email("Must be an email"),
  },
  {
    key: "password",
    value: "",
    label: "Password: ",
    control: "password",
    validation: Yup.string("Password must be a string").required(
      "Password is required"
    ),
  },
];
export default loginFormSchema;
