import * as Yup from "yup";
const registerFormGuestUserSectionSchema = [
  {
    key: "email",
    value: "",
    label: "Email: ",
    control: "text",
    validation: Yup.string("Email must be a string")
      .required("Email is required")
      .email("Must be an email"),
  },
  {
    key: "name",
    value: "",
    label: "Name: ",
    control: "text",
    validation: Yup.string("Name must be a string").required(
      "Name is required"
    ),
  },
  {
    key: "phone_number",
    value: "",
    label: "Phone Number: ",
    control: "text",
    validation: Yup.string("Phnome number must be a string").required(
      "Phone number is required"
    ),
  },
];
export default registerFormGuestUserSectionSchema;
