import * as Yup from "yup";
const registerFormAddressSectionSchema = [
  {
    key: "address_1",
    value: "",
    label: "Address 1: ",
    control: "text",
    validation: Yup.string("Email must be a string").required(
      "Address 1 is required"
    ),
  },
  {
    key: "address_2",
    value: "",
    label: "Address 2: ",
    control: "text",
    validation: Yup.string("Address 2 must be a string"),
  },
  {
    key: "city",
    value: "",
    label: "City: ",
    control: "text",
    validation: Yup.string("City must be a string").required(
      "City is required"
    ),
  },
  {
    key: "district",
    value: "",
    label: "District: ",
    control: "select",
    validation: Yup.string("District must be a string").required(
      "District is required"
    ),
    options: [
      {
        key: "Chamkar Mon",
        value: "chamkar-mon",
      },
      {
        key: "Daun Penh",
        value: "daun-penh",
      },
      {
        key: "Prampir Makara",
        value: "prampir-makara",
      },
      {
        key: "Toul Kork",
        value: "toul-kork",
      },
      {
        key: "Dangkao",
        value: "dangkao",
      },
      {
        key: "Mean Chey",
        value: "mean-chey",
      },
      {
        key: "Russey Keo",
        value: "russey-keo",
      },
      {
        key: "Sen Sok",
        value: "sen-sok",
      },
      {
        key: "Pou Senchey",
        value: "pou-senchey",
      },
      {
        key: "Chroy Changvar",
        value: "chroy-changvar",
      },
      {
        key: "Prek Pnov",
        value: "prek-pnov",
      },
      {
        key: "Chbar Ampov",
        value: "chbar-ampov",
      },
      {
        key: "Boeng Keng Kang",
        value: "boeng-keng-kang",
      },
      {
        key: "Kamboul",
        value: "kamboul",
      },
    ],
  },
];
export default registerFormAddressSectionSchema;
