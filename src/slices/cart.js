import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  myCart: JSON.parse(localStorage.getItem("myCart")) || {},
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart: (state, params) => {
      const { productID, amount } = params.payload;
      if (state.myCart[productID]) {
        state.myCart[productID] += amount;
      } else {
        state.myCart[productID] = amount;
      }
    },
    clearCart: (state) => {
      localStorage.removeItem("myCart");
    },
  },
});

export const { addToCart, clearCart } = cartSlice.actions;

export const selectCart = (state) => state.cart.myCart;
export default cartSlice.reducer;
