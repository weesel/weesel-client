import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  fetchAuthenticatedUser,
  login,
  logout,
  removeAccessTokenFromStorage,
  setAccessTokenToStorage,
} from "../utils/const";

const initialState = {
  access_token: JSON.parse(localStorage.getItem("user_access_token")) || null,
  remember_me: false,
  user: null,
};

export const checkAuth = createAsyncThunk("auth/checkAuth", async (token) => {
  const response = await fetchAuthenticatedUser();
  return response;
});

export const loginWithRedirect = createAsyncThunk(
  "auth/login",
  async (payload) => {
    return login(payload.loginCredentials)
      .then((response) => {
        console.log(response);
        return {
          user: response.user,
          redirectCallback: payload.redirectCallback,
        };
      })
      .catch((error) => {
        console.log(error.response);
        return error;
      });
  }
);

export const logoutWithRedirect = createAsyncThunk(
  "auth/logout",
  async (payload) => {
    return logout(payload.token).then((response) => {
      console.log(response);
      return {
        redirectCallback: payload.redirectCallback,
      };
    });
  }
);

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {},
  extraReducers: {
    [checkAuth.rejected]: (state, action) => {
      state.user = null;
    },
    [checkAuth.fulfilled]: (state, action) => {
      state.user = action.payload.user;
    },
    [loginWithRedirect.fulfilled]: (state, action) => {
      // setAccessTokenToStorage(action.payload.access_token);
      action.payload.redirectCallback();
    },
    [logoutWithRedirect.fulfilled]: (_, action) => {
      action.payload.redirectCallback();
    },
  },
});

export const selectAccessToken = (state) => state.auth.access_token;
export const selectUser = (state) => state.auth.user;

export default authSlice.reducer;
