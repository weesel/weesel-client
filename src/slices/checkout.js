import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  checkoutStep: 1,
};

export const checkoutSlice = createSlice({
  name: "checkout",
  initialState,
  reducers: {
    nextCheckoutStep: (state) => {
      state.checkoutStep = state.checkoutStep + 1;
    },
  },
});

export const { nextCheckoutStep } = checkoutSlice.actions;

export const selectCheckoutStep = (state) => state.checkout.checkoutStep;
export default checkoutSlice.reducer;
