import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  registerData: {},
};

export const registerSlice = createSlice({
  name: "register",
  initialState,
  reducers: {
    setRegisterData: (state, registerData) => {
      state.registerData = {
        ...state.registerData,
        ...registerData.payload,
      };
    },
  },
});

export const { setRegisterData } = registerSlice.actions;

export const selectRegisterData = (state) => state.register.registerData;

export default registerSlice.reducer;
