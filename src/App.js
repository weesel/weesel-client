/**
 * node_module import
 */
import React, { useEffect } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
/**
 * pages import
 */
import Home from "pages/Home";
import Login from "pages/Login";
import Register from "pages/Register";
import PublicRoute from "./routes/PublicRoute";
import CategoryProduct from "pages/CategoryProduct";
import Myprofile from "pages/Myprofile";
import ProductResult from "pages/ProductResult";
import MyCart from "pages/MyCart";
import ProductDetail from "pages/ProductDetail";
import Checkout from "pages/Checkout";
import Product from "pages/ProductResult";
/**
 * components import
 */
import Nav from "components/Nav";
import NavTop from "components/NavTop";
import Footer from "components/Footer";
/**
 * slices import
 */
import { checkAuth, selectAccessToken } from "slices/auth";

function App() {
  const access_token = useSelector(selectAccessToken);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(checkAuth(access_token));
  }, [dispatch]);
  return (
    <div className="App">
      <BrowserRouter forceRefresh={true}>
        <div
          style={{
            position: "sticky",
            top: "0",
            zIndex: "9999",
          }}
        >
          <NavTop />
          <Nav />
        </div>
        <Switch>
          <Route exact path="/" render={() => <Home />} />
          <PublicRoute path="/register" children={<Register />} />
          <PublicRoute path="/login" children={<Login />} />
          <Route
            path="/categories/:id/products"
            children={<CategoryProduct />}
          />
          <Route path="/my-profile" render={() => <Myprofile />} />
          <Route path="/my-cart" render={() => <MyCart />} />
          <Route path="/product/:id" children={<ProductDetail />} />
          <Route path="/checkout" children={<Checkout />} />
          <Route path="/product" render={() => <Product />} />
          <Route path="/products" render={()=><ProductResult/>}/>

          {/* <Route path="/order" render={() => <Order />} /> */}
          {/* <Route path="/products" render={() => <Product />} /> */}
          {/* <ProtectedRoute
            authenticated = {authenticated}
            path = "/my-favourite"
            children = {<MyFavourite/>}
          />
          <ProtectedRoute
            authenticated = {authenticated}
            path = "/my-cart"
            children = {<MyCart/>}
          />
          //order display history of what customer used to order
          <ProtectedRoute
            authenticated = {authenticated}
            path = "/order"
            children = {<Order/>}
          /> */}
        </Switch>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
