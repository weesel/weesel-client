import { configureStore } from "@reduxjs/toolkit";
import authReducer from "slices/auth";
import cartReducer from "slices/cart";
import registerReducer from "slices/register";
import checkoutReducer from "slices/checkout";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    cart: cartReducer,
    register: registerReducer,
    checkout: checkoutReducer,
  },
});
