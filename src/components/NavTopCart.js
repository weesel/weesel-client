import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { selectCart } from "slices/cart";
import { Link } from "react-router-dom";
function NavTopCart() {
  const myCart = useSelector(selectCart);
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    if (Object.keys(myCart).length > 0) {
      localStorage.setItem("myCart", JSON.stringify(myCart));
      setCounter(Object.values(myCart).reduce((acc, cur) => acc + cur, 0));
    }
  }, [myCart]);
  return (
    <Link to="/my-cart">
      <div className="navtop-cart">
        <div className="navtop-cart-counter">{counter}</div>
        <i className="fas fa-shopping-cart"></i>
      </div>
    </Link>
  );
}

export default NavTopCart;
