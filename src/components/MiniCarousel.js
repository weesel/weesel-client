import React, { useRef } from "react";
import ReactOwlCarousel from "react-owl-carousel";
// import "owl.carousel/dist/assets/owl.carousel.css";
// import "owl.carousel/dist/assets/owl.theme.default.css";

import "css/MiniCarousel.css";

function MiniCarousel({ images }) {
  const carousel = useRef();
  const next = () => carousel.current.next(500);
  const prev = () => carousel.current.prev(500);
  return (
    <div style={{ position: "relative" }}>
      <ReactOwlCarousel ref={carousel} {...reactOwlCarouselOptions}>
        {images.map((image, index) => (
          <img
            style={{ width: "5rem", height: "auto" }}
            src={image}
            alt={`slide-${index}`}
          />
        ))}
      </ReactOwlCarousel>
      {images.length > 1 && (
        <React.Fragment>
          <div onClick={prev} className="mini-carousel-left">
            <i className="fas fa-chevron-left"></i>
          </div>
          <div onClick={next} className="mini-carousel-right">
            <i className="fas fa-chevron-right"></i>
          </div>
        </React.Fragment>
      )}
    </div>
  );
}

const reactOwlCarouselOptions = {
  items: 1,
};

export default MiniCarousel;
