import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { fetchCategoriesWithChildren } from "utils/const";

import "css/CategoryList.css";
function CategoryList() {
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    fetchCategoriesWithChildren()
      .then((categories) => setCategories(categories))
      .catch((error) => console.log(error));
  }, []);
  return (
    <ul className="category-list">
      {categories.map((category) => (
        <li key={category.id} className="category-listItem">
          <Link
            to={{
              pathname: `/categories/${category.id}/products`,
              search: "?page=1",
              state: {
                categoryName: category.name,
              },
            }}
          >
            {category.name}
          </Link>
          {category.children.length > 0 && (
            <React.Fragment>
              <i onClick={expandChildren} className="fa fa-chevron-right" />
              <ul className="category-children-list">
                {category.children.map((childrenCategory) => (
                  <li key={childrenCategory.id}>
                    <Link
                      to={{
                        pathname: `/categories/${category.id}/products`,
                        search: `?subcategory=${childrenCategory.id}`,
                        search: "?page=1",
                      }}
                    >
                      {childrenCategory.name}
                    </Link>
                  </li>
                ))}
              </ul>
            </React.Fragment>
          )}
        </li>
      ))}
    </ul>
  );
}

const expandChildren = (e) => {
  const parentList = e.target.parentElement.parentElement;
  const selectedList = e.target.parentElement;
  /**
   * remove all other selected
   */
  Array.from(parentList.children).forEach((element) => {
    if (
      element !== selectedList &&
      element.querySelector(".category-children-list")
    ) {
      if (
        element
          .querySelector(".category-children-list")
          .classList.contains("show")
      ) {
        element
          .querySelector(".category-children-list")
          .classList.remove("show");
        element.querySelector("i").classList.remove("fa-chevron-down");
        element.querySelector("i").classList.add("fa-chevron-right");
      }
    }
  });

  /**
   * add selected to the current list
   */
  const children = e.target.nextSibling;
  children.classList.toggle("show");
  if (children.classList.contains("show")) {
    e.target.classList.remove("fa-chevron-right");
    e.target.classList.add("fa-chevron-down");
  } else {
    e.target.classList.remove("fa-chevron-down");
    e.target.classList.add("fa-chevron-right");
  }
};

export default CategoryList;
