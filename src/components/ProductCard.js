import React from "react";
import "css/ProductCard.css";

function ProductCard({ title, rating, price, discount, image }) {
  return (
    <div className="product-card">
      <div className="product-card-image">
        <img src={image} alt="product" />
      </div>
      <div className="product-card-title">
        <span>{title}</span>
      </div>
      <div className="product-card-extra">
        <div className="product-card-ratings">
          {Array.from(Array(rating).keys()).map((_, i) => (
            <span key={`star-${i}`} className="fa fa-stack">
              <i className="fa fa-star fa-stack-2x"></i>
            </span>
          ))}
          {Array.from(Array(5 - rating).keys()).map((_, i) => (
            <span key={`star-o-${i}`} className="fa fa-stack">
              <i className="fa fa-star-o fa-stack-2x"></i>
            </span>
          ))}
        </div>
        <div>
          {discount === 0 ? (
            <div>
              <span>${price.toFixed(2)}</span>
            </div>
          ) : (
            <div className="product-card-price">
              <div className="price-line-through">
                <span>${price.toFixed(2)}</span>
              </div>
              <div>
                <span>${(price - (price * discount) / 100).toFixed(2)}</span>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default ProductCard;
