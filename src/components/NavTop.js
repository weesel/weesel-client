import React from "react";
import { Link, useHistory } from "react-router-dom";
import "css/NavTop.css";
import NavTopCart from "components/NavTopCart";
import { useSelector, useDispatch } from "react-redux";
import { logoutWithRedirect, selectUser, selectAccessToken } from "slices/auth";

function NavTop() {
  const user = useSelector(selectUser);
  const access_token = useSelector(selectAccessToken);
  const dispatch = useDispatch();
  const history = useHistory();
  return (
    <nav className="navtop">
      <div className="navtop-left">
        <i className="fab fa-shopify"></i>
        <span>Wants to explore Upcoming Deals on Weekends?</span>
      </div>
      <div className="navtop-right">
        <div className="navtop-right-option">
          <span>Language</span>
          <i onClick={dropdownHandler} className="fas fa-chevron-down"></i>
          <Dropdown
            options={[
              {
                label: "English",
                url: "#",
              },
              {
                label: "Khmer",
                url: "#",
              },
            ]}
          />
        </div>
        <div className="navtop-right-option">
          <span>Currency</span>
          <i onClick={dropdownHandler} className="fas fa-chevron-down"></i>
          <Dropdown
            options={[
              { label: "Dollar", url: "#" },
              { label: "Riel", url: "#" },
            ]}
          />
        </div>
        <div className="navtop-right-option">
          <span>My Account</span>
          <i onClick={dropdownHandler} className="fas fa-chevron-down"></i>
          {user ? (
            <Dropdown
              options={[
                { label: "My Account", url: "/my-profile" },
                {
                  label: "Logout",
                  isButton: true,
                  onClick: () =>
                    dispatch(
                      logoutWithRedirect({
                        token: access_token,
                        redirectCallback: () => history.push("/"),
                      })
                    ),
                },
              ]}
            />
          ) : (
            <Dropdown
              options={[
                { label: "Register", url: "/register" },
                { label: "Login", url: "/login" },
              ]}
            />
          )}
        </div>
        <div className="navtop-right-option">
          <span>Wish List</span>
        </div>
        <NavTopCart />
      </div>
    </nav>
  );
}

const Dropdown = ({ options }) => {
  return (
    <ul className="navtop-dropdown">
      {options.map((option, index) =>
        option.isButton ? (
          <button onClick={option.onClick}>
            <li>{option.label}</li>
          </button>
        ) : (
          <Link key={index} to={option.url}>
            <li style={{ whiteSpace: "nowrap" }}>{option.label}</li>
          </Link>
        )
      )}
    </ul>
  );
};

const dropdownHandler = (e) => {
  // current selected dropdown
  const dropdownElem = e.target.parentElement.querySelector(".navtop-dropdown");
  /**
   * remove show from every dropdown first
   */
  Array.from(
    document.querySelector(".navtop").querySelectorAll(".navtop-dropdown")
  ).forEach((dropdown) => {
    if (dropdown.classList.contains("show") && dropdown !== dropdownElem)
      dropdown.classList.remove("show");
  });
  /**
   * show dropdown on selected
   */
  dropdownElem.classList.toggle("show");
};

export default NavTop;
