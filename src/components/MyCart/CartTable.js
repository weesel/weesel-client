import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { selectCart } from "slices/cart";
import { fetchProduct } from "utils/const";

function CartTable({ other = null }) {
  const history = useHistory();
  const myCart = useSelector(selectCart);
  const [cart, setCart] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [totalDiscount, setTotalDiscount] = useState(0);

  useEffect(() => {
    Object.keys(myCart).forEach((id) => {
      fetchProduct(id)
        .then((product) => {
          product["cartCount"] = myCart[id];
          setCart((prevState) => [...prevState, product]);
        })
        .catch((error) => console.log(error));
    });
  }, [myCart]);

  useEffect(() => {
    setTotalPrice(
      cart.reduce((acc, cur) => acc + cur.price * cur.cartCount, 0)
    );
    setTotalDiscount(
      cart.reduce(
        (acc, cur) => acc + cur.cartCount * ((cur.price * cur.discount) / 100),
        0
      )
    );
  }, [cart]);
  return (
    <table className="mycart-table">
      <tr className="mycart-table-row">
        <th className="mycart-table-header">Image</th>
        <th className="mycart-table-header">Product Name</th>
        <th className="mycart-table-header">Product ID</th>
        <th className="mycart-table-header">Unit Price</th>
        <th className="mycart-table-header">Quantity</th>
        <th className="mycart-table-header">Total</th>
      </tr>
      {cart.map((product) => (
        <tr key={product.id} className="mycart-table-row">
          <td className="mycart-table-data">
            <img
              style={{ maxWidth: "5rem", height: "auto" }}
              src={product.images[0].url}
              alt={`product-${product.id}`}
            />
          </td>
          <td className="mycart-table-data">{product.name}</td>
          <td className="mycart-table-data">{product.id}</td>
          <td className="mycart-table-data">
            <span
              style={{
                textDecoration: product.discount > 0 ? "line-through" : "none",
              }}
            >
              ${product.price.toFixed(2)}
            </span>
            {product.discount > 0 && (
              <span style={{ marginLeft: "8px" }}>
                ${((product.price * (100 - product.discount)) / 100).toFixed(2)}
              </span>
            )}{" "}
          </td>
          <td className="mycart-table-data">
            {product.cartCount}</td>
          <td className="mycart-table-data">
            $ {(product.cartCount * product.price).toFixed(2)}
          </td>
        </tr>
      ))}
      <tr className="mycart-table-row">
        <th className="mycart-table-data">Total Price</th>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td className="mycart-table-data">${totalPrice.toFixed(2)}</td>
      </tr>
      <tr className="mycart-table-row">
        <th className="mycart-table-data">Discount</th>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td className="mycart-table-data">${totalDiscount.toFixed(2)}</td>
      </tr>
      <tr className="mycart-table-row">
        <th className="mycart-table-data">Total Price After Discount</th>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td className="mycart-table-data">
          $ {(totalPrice - totalDiscount).toFixed(2)}
        </td>
      </tr>
      {other}
    </table>
  );
}

export default CartTable;
