import React from "react";
import InputTest from "./InputText";
import InputPassword from "./InputPassword";
import Textarea from "./Textarea";
import Select from "./Select";
import RadioButton from "./RadioButton";
import CheckboxGroup from "./CheckboxGroup";

function FormikControl(props) {
  const { control, ...rest } = props;
  switch (control) {
    case "text":
      return <InputTest {...rest} />;
    case "password":
      return <InputPassword {...rest} />;
    case "textarea":
      return <Textarea {...rest} />;
    case "select":
      return <Select {...rest} />;
    case "radio":
      return <RadioButton {...rest} />;
    case "checkbox":
      return <CheckboxGroup {...rest} />;
    default:
      return null;
  }
}

export default FormikControl;
