import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  registerFormGuestUserSectionSchema,
  registerFormAddressSectionSchema,
} from "utils/FormSchema";
import { selectRegisterData, setRegisterData } from "slices/register";
import Form from "components/Forms/Form";
import "css/Register.css";

function RegisterForm() {
  const dispatch = useDispatch();
  const registerData = useSelector(selectRegisterData);
  const formHandler = (payload) => dispatch(setRegisterData(payload));
  return (
    <div className="register-form">
      <div className="register-form-section">
        <p className="register-form-title">User Information</p>
        <Form
          fields={registerFormGuestUserSectionSchema}
          submitHandler={formHandler}
          children={
            !registerData.name && (
              <button className="register-form-btn" type="submit">
                Continue
              </button>
            )
          }
        />
      </div>
      <div className="register-form-section">
        <p className="register-form-title">User Address</p>
        {registerData.name && (
          <React.Fragment>
            <Form
              fields={registerFormAddressSectionSchema}
              submitHandler={formHandler}
              children={
                !registerData.address_1 && (
                  <button className="register-form-btn" type="submit">
                    Continue
                  </button>
                )
              }
            />
          </React.Fragment>
        )}
      </div>
    </div>
  );
}

export default RegisterForm;
