import React from "react";
import { Formik, Form } from "formik";
import FormikControl from "../Formik/FormikControl";
import * as Yup from "yup";
/**
 * @param {array} fields
 * [
 *    {
 *      key: key
 *      value: value,
 *      label: label,
 *      validation: validation,
 *      control: field-control
 *    }
 * ]
 */

// eslint-disable-next-line
export default ({ fields, submitHandler, children }) => {
  const initialValues = fields.reduce((acc, field) => {
    acc[field.key] = field.value;
    return acc;
  }, {});
  const validationSchema = Yup.object(
    fields.reduce((acc, field) => {
      acc[field.key] = field.validation;
      return acc;
    }, {})
  );
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={submitHandler}
    >
      {(formik) => (
        <Form>
          {fields.map((field) => (
            <FormikControl
              key={field.key}
              control={field.control}
              label={field.label}
              name={field.key}
              options={field.options}
            />
          ))}
          {children}
        </Form>
      )}
    </Formik>
  );
};
