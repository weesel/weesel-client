import React from "react";
import "css/Footer.css";

function Footer() {
  return (
    <footer className="footer">
      <div className="footer-sections">
        <div className="footer-about-store">
          <span className="footer-section-title">About Store</span>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque
            reprehenderit expedita exercitationem sunt recusandae tenetur ad
            earum excepturi.
          </p>
        </div>
        <div className="footer-extras">
          <span className="footer-section-title">Extras</span>
          <ul>
            <li>Brands</li>
            <li>Gift Certificates</li>
            <li>Affiliates</li>
            <li>Contact Us</li>
            <li>Specials</li>
          </ul>
        </div>
        <div className="footer-my-account">
          <span className="footer-section-title">My Account</span>
          <ul>
            <li>My Account</li>
            <li>Order History</li>
            <li>Wish List</li>
            <li>Newsletter</li>
            <li>Returns</li>
          </ul>
        </div>
        <div className="footer-store-information">
          <span className="footer-section-title">Store Information</span>
          <div style={{ display: "flex" }}>
            <i
              style={{ marginRight: "1rem" }}
              className="fas fa-map-marker-alt"
            ></i>
            <p>My Company, 42 Puffin street 12345 Puffinville France</p>
          </div>
          <div style={{ display: "flex" }}>
            <i style={{ marginRight: "1rem" }} className="fas fa-phone"></i>
            <p>0123-456-789</p>
          </div>
          <div style={{ display: "flex" }}>
            <i style={{ marginRight: "1rem" }} className="fas fa-envelope"></i>
            <p>sales@yourcompany.com</p>
          </div>
        </div>
      </div>
      <div className="footer-payment-options">
        <span>Payment Accepted By Store</span>
        <div style={{ display: "flex", marginTop: "0.5rem" }}>
          <div>
            <img
              src="https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC02/image/catalog/demo/banners/visa.png"
              alt="visa"
            />
          </div>
          <div>
            <img
              src="https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC02/image/catalog/demo/banners/mastercard.png"
              alt="mastercard"
            />
          </div>
          <div>
            <img
              src="https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC02/image/catalog/demo/banners/discover.png"
              alt="discover"
            />
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
