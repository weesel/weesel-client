import React from "react";
import CartTable from "components/MyCart/CartTable";

function Receipt({ deliveryPrice }) {
  return (
    <div>
      <CartTable
        other={
          <tr className="mycart-table-row">
            <th className="mycart-table-header">Delivery Price: </th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td className="mycart-table-data">$ {deliveryPrice}</td>
          </tr>
        }
      />
    </div>
  );
}

export default Receipt;
