import React from "react";

function Rating({ rating }) {
  return (
    <React.Fragment>
      {Array.from(Array(parseInt(rating, 10))).map((_, index) => (
        <span key={index} className="fa fa-stack">
          <i className="fa fa-star fa-stack-2x"></i>
        </span>
      ))}
      {Array.from(Array(5 - parseInt(rating, 10))).map((_, index) => (
        <span key={index} className="fa fa-stack">
          <i className="fa fa-star-o fa-stack-2x"></i>
        </span>
      ))}
    </React.Fragment>
  );
}

export default Rating;
