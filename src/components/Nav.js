import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "css/Nav.css";
import { fetchCategoriesWithChildren } from "utils/const";

function Nav() {
  const [categories, setCategories] = useState([]);
  const [queryProductName, setQueryProductName] = useState("");
  const [queryCategory, setQueryCategory] = useState("all");
  useEffect(() => {
    fetchCategoriesWithChildren()
      .then((categories) => setCategories(categories))
      .catch((error) => console.log(error));
  }, []);
  return (
    <nav className="nav">
      <Link to="/">
        <img
          style={{ margin: "0.5rem" }}
          src="https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC05/image/catalog/logo.png"
          alt="logo.png"
        />
      </Link>
      <div className="nav-search">
        <input
          onChange={(e) => setQueryProductName(e.target.value)}
          type="text"
          name="product"
          id="product"
        />
        <select
          onChange={(e) => setQueryCategory(e.target.value)}
          name="categories"
          id="categories"
        >
          <option value="all">ALL</option>
          {categories.map((category) => (
            <option key={category.id} value={category.id}>
              {category.name.toUpperCase()}
            </option>
          ))}
        </select>
        <Link
          style={{
            pointerEvents: queryProductName.length > 0 ? "all" : "none",
          }}
          to={{
            pathname: "/products",
            search: `?name=${queryProductName}&category=${queryCategory}`,
          }}
        >
          Search
        </Link>
      </div>

      <div className="mobile-home-categories">
        <div className="mobile-home-categories-left">
          <span>Categories</span>
        </div>
        <div className="mobile-home-categories-right">
          <i onClick={expandMobileCategories} className="fas fa-bars" />
          <div className="mobile home-categories-cart">
            <i className="fas fa-shopping-cart"></i>
          </div>
        </div>
        <div className="mobile-home-categories-dropdown">
          <ul className="home-category-list">
            {categories.map((category) => (
              <li key={category.id} className="home-category-listItem">
                <Link to={`/categories/${category.id}/products`}>
                  {category.name}
                </Link>
                {category.children.length > 0 && (
                  <React.Fragment>
                    <i
                      onClick={expandChildren}
                      className="fa fa-chevron-right"
                    />
                    <ul className="home-category-children-list">
                      {category.children.map((childrenCategory) => (
                        <li key={childrenCategory.id}>
                          {childrenCategory.name}
                        </li>
                      ))}
                    </ul>
                  </React.Fragment>
                )}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </nav>
  );
}

const expandMobileCategories = () => {
  const dropdown = document.querySelector(".mobile-home-categories-dropdown");
  console.log(dropdown);
  dropdown.classList.toggle("show");
};

const expandChildren = (e) => {
  const children = e.target.nextSibling;
  children.classList.toggle("show");
};

export default Nav;
