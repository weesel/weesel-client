import React from "react";
import "css/Benefit.css";

function Benefit() {
  return (
    <div className="benefit">
      <div className="all-ser text-center">
        <div className="ser-block">
          <div className="d-inline-block">
            <i className="fa fa-smile-o"></i>
          </div>
          <div className="ser-co d-inline-block text-left">
            <h5>100% Satification</h5>
            <h6>If you are unable.</h6>
          </div>
        </div>
        <div className="ser-block">
          <div className="d-inline-block">
            <i className="fa fa-money"></i>
          </div>
          <div className="ser-co d-inline-block text-left">
            <h5>Save 20% when you</h5>
            <h6>use Credit Card.</h6>
          </div>
        </div>
        <div className="ser-block">
          <div className="d-inline-block">
            <i className="fa fa-motorcycle"></i>
          </div>
          <div className="ser-co d-inline-block text-left">
            <h5>Fast Free Delivery</h5>
            <h6>$10+</h6>
          </div>
        </div>
        <div className="ser-block">
          <div className="d-inline-block">
            <i className="fa fa-retweet"></i>
          </div>
          <div className="ser-co d-inline-block text-left">
            <h5>1 Day money back</h5>
            <h6>If you are unable.</h6>
          </div>
        </div>
        <div className="ser-block">
          <div className="d-inline-block">
            <i className="fa fa-credit-card"></i>
          </div>
          <div className="ser-co d-inline-block text-left">
            <h5>Payment method</h5>
            <h6>Secure Payment</h6>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Benefit;
