import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";

export default function AdvertisementSlider() {
  return (
    <Swiper
      spaceBetween={30}
      centeredSlides={true}
      autoplay={{
        delay: 2500,
        disableOnInteraction: false,
      }}
      pagination={{
        clickable: true,
      }}
      navigation={true}
      style={{ maxWidth: "100%", maxHeight: "430px", background: "white" }}
      className="mySwiper"
    >
      {advertisementImages.map((image, index) => (
        <SwiperSlide key={index} style={{ maxWidth: "100%", height: "100%" }}>
          <img
            style={{
              width: "100%",
              height: "100%",
              objectFit: "fit",
              objectPosition: "center",
            }}
            src={image}
            alt={`slide-${index}`}
          />
        </SwiperSlide>
      ))}
    </Swiper>
  );
}

const advertisementImages = [
  "https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC05/image/cache/catalog/demo/banners/slider-01-605x430.jpg",
  "https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC05/image/cache/catalog/demo/banners/slider-02-605x430.jpg",
];
