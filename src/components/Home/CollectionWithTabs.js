import React, { useEffect, useState } from "react";
import ProductCarousel from "components/Home/ProductCarousel";
import { fetchProductCollections, capitalizeFirstLetter } from "utils/const";

const CollectionWithTabs = ({ tabs }) => {
  const [active, setActive] = useState("featured");
  const [collections, setCollections] = useState([]);
  useEffect(() => {
    fetchProductCollections()
      .then((collections) => setCollections(collections))
      .catch((error) => console.log(error));
  }, []);

  return (
    <div className="home-collection">
      <div className="home-collection-tabs">
        {tabs.map((tab, index) => (
          <span
            key={index}
            className={active === tab ? "home-collection-tab-active" : ""}
            onClick={() => setActive(tab)}
          >
            {capitalizeFirstLetter(tab)}
          </span>
        ))}
      </div>
      <div className="home-collection-products">
        <section>
          {collections.length > 0 && (
            <ProductCarousel
              products={
                collections.find((collection) => collection.name === active)
                  .products
              }
            />
          )}
        </section>
      </div>
    </div>
  );
};

export default CollectionWithTabs;
