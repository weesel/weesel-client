import React, { useRef } from "react";
import ProductCard from "components/ProductCard";
import ReactOwlCarousel from "react-owl-carousel";
import { Link } from "react-router-dom";

const reactOwlCarouselOptions = {
  responsive: {
    300: {
      items: 1,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 4,
    },
  },
  margin: 0,
  dots: false,
};

function ProductCarousel({ products }) {
  const productCarousel = useRef();
  // const next = () => productCarousel.current.next(500);
  // const prev = () => productCarousel.current.prev(500);
  return (
    <div style={{ position: "relative", paddingTop: "1rem" }}>
      <ReactOwlCarousel
        style={{ backgroundColor: "white" }}
        {...reactOwlCarouselOptions}
        ref={productCarousel}
      >
        {products.map((product, index) => (
          <Link key={index} to={`/product/${product.id}`}>
            <ProductCard
              title={product.name}
              rating={product.rating}
              price={product.price}
              discount={product.discount}
              image={product.images[0].url}
            />
          </Link>
        ))}
      </ReactOwlCarousel>
    </div>
  );
}

export default ProductCarousel;
