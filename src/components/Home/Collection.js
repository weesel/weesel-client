import React, { useEffect, useState } from "react";
import ProductCarousel from "components/Home/ProductCarousel";
import {
  fetchProductCollections,
  capitalizeFirstLetter,
} from "../../utils/const";

const Collection = ({ title }) => {
  const [collections, setCollections] = useState([]);
  useEffect(() => {
    fetchProductCollections()
      .then((collections) => setCollections(collections))
      .catch((error) => console.log(error));
  }, []);

  return (
    <div className="home-collection">
      <span>{capitalizeFirstLetter(title)}</span>
      <div className="home-collection-products">
        <section>
          {collections.length > 0 && (
            <ProductCarousel
              products={
                collections.find((collection) => collection.name === title)
                  .products
              }
            />
          )}
        </section>
      </div>
    </div>
  );
};
export default Collection;
