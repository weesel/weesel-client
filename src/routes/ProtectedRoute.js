import React from "react";
import { Redirect, Route } from "react-router-dom";

function ProtectedRoute({ path, authenticated, children }) {
  return (
    <div>
      <Route
        path={path}
        render={() => (authenticated ? children : <Redirect to="/" />)}
      />
    </div>
  );
}

export default ProtectedRoute;
