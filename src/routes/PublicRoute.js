import React from "react";
import { Redirect, Route } from "react-router-dom";

function PublicRoute({ path, authenticated, children }) {
  return (
    <div>
      <Route
        path={path}
        render={() => (authenticated ? <Redirect to="/" /> : children)}
      />
    </div>
  );
}

export default PublicRoute;
