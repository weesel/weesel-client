import React from 'react';
import { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

const PublicRoute = ({ render: Component, ...rest }) => {
    return (
        <Route {...rest} render={(props) => (
            <div>
                <Component {...props} />
            </div>
        )}
        />

    );
}

export default PublicRoute;

